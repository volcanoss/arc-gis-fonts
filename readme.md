# ArcGIS Javascript Font


`FeatureLayer`等图层中使用的`Label`的字体，如果使用还是从远程服务器下载，建议自己独立部署

## 包括以下字体

- arial-unicode-ms-regular

- arial-unicode-ms-bold    

- noto-serif-regular       

- noto-serif-bold          

- playfair-display-regular 

- playfair-display-bold    

- microsoft-yahei-regular 

- microsoft-yahei-bold     

- simsun-regular           

- simsun-bold        

